import os

from celery import Celery

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'notifications.settings')

app = Celery('notifications')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')
import pytz

app.conf.timezone = 'UTC'
app.conf.enable_utc = True

local_timezone = pytz.timezone('Europe/Moscow')

# Load task modules from all registered Django apps.
app.autodiscover_tasks()
