from django.test import TestCase
from django.core.exceptions import ValidationError
from .models import Client, Broadcast, Message


class ClientTestCase(TestCase):
    def setUp(self):
        self.client = Client.objects.create(phone_number='8234567890', mobile_operator_code='723', tag='test',
                                            timezone='UTC')

    def test_client_phone_number_validation(self):
        invalid_client = Client(phone_number='723456', mobile_operator_code='723', tag='test', timezone='UTC')
        try:
            self.assertFalse(invalid_client.full_clean())
        except ValidationError:
            self.fail('ValidationError not raised')

        valid_client = Client(phone_number='7234567890', mobile_operator_code='723', tag='test', timezone='UTC')
        self.assertIsNone(valid_client.full_clean())
        try:
            valid_client.full_clean()
        except ValidationError:
            self.fail('ValidationError raised')

    def test_client_save_method(self):
        self.assertEqual(self.client.operator_code, '823')


class BroadcastModelTestCase(TestCase):
    def setUp(self):
        self.broadcast = Broadcast.objects.create(
            start_time='2022-12-21T00:00:00Z',
            message_text='Test broadcast message',
            operator_code='123456',
            tag='test',
            end_time='2022-12-22T00:00:00Z',
        )

    def test_broadcast_creation(self):
        broadcast = Broadcast.objects.get(id=self.broadcast.id)

        self.assertEqual(broadcast.start_time.strftime('%Y-%m-%dT%H:%M:%SZ'), '2022-12-21T00:00:00Z')
        self.assertEqual(broadcast.message_text, 'Test broadcast message')
        self.assertEqual(broadcast.operator_code, '123456')
        self.assertEqual(broadcast.tag, 'test')
        self.assertEqual(broadcast.end_time.strftime('%Y-%m-%dT%H:%M:%SZ'), '2022-12-22T00:00:00Z')

    def test_str_method(self):
        broadcast = Broadcast.objects.get(id=self.broadcast.id)

        self.assertEqual(str(broadcast), 'Broadcast 4')

    def test_end_time_after_start_time(self):
        with self.assertRaises(ValidationError):
            broadcast = Broadcast(
                start_time='2022-12-22T00:00:00Z',
                message_text='Test broadcast message',
                operator_code='123456',
                tag='test',
                end_time='2022-12-21T00:00:00Z',
            )
            broadcast.full_clean()

    def test_operator_code_length(self):
        broadcast = Broadcast(
            start_time='2022-12-21T00:00:00Z',
            message_text='Test broadcast message',
            operator_code='123456789',
            tag='test',
            end_time='2022-12-22T00:00:00Z',
        )
        broadcast.operator_code = '1234567890'
        self.assertEqual(len(broadcast.operator_code), 10)


class MessageModelTestCase(TestCase):
    def setUp(self):
        self.client = Client.objects.create(phone_number='1234567890', mobile_operator_code='123', tag='test_client',
                                            timezone='UTC')
        self.broadcast = Broadcast.objects.create(start_time='2022-12-21T00:00:00Z', message_text='Test message',
                                                  operator_code='123', tag='test_broadcast', end_time='2022-12-21T01'
                                                                                                      ':00:00Z')

    def test_str_method(self):
        message = Message.objects.create(broadcast=self.broadcast, client=self.client)
        self.assertEqual(str(message), "Message {}".format(message.id))

    def test_save_method(self):
        client = Client.objects.create(phone_number='7234567890', mobile_operator_code='', tag='test_client',
                                       timezone='UTC')
        self.assertEqual(client.operator_code, '723')

    def test_message_statuses(self):
        message = Message.objects.create(broadcast=self.broadcast, client=self.client, status='queued')
        self.assertEqual(message.status, 'queued')
        message.status = 'invalid'
        self.assertRaises(ValidationError, message.clean_fields)
