import logging

logging.basicConfig(
    filename='broadcasts.log',
    filemode='a',
    format='%(asctime)s - %(levelname)s - %(message)s',
    level=logging.INFO
)