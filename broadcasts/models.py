from datetime import datetime

from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone

from .validators import validate_phone_number


class Client(models.Model):
    phone_number = models.CharField(max_length=11, unique=True, validators=[validate_phone_number])
    mobile_operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=50, blank=True)
    timezone = models.CharField(max_length=50)

    def __str__(self):
        return self.phone_number

    def save(self, *args, **kwargs):
        if not self.phone_number:
            return

        self.operator_code = str(self.phone_number[:3])
        super().save(*args, **kwargs)


class Broadcast(models.Model):
    start_time = models.DateTimeField()
    message_text = models.TextField()
    operator_code = models.CharField(max_length=10, blank=True)
    tag = models.CharField(max_length=50, blank=True)
    end_time = models.DateTimeField()

    def __str__(self):
        return f"Broadcast {self.id}"

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        if self.end_time <= self.start_time:
            raise ValidationError({'end_time': 'End time must be after start time'})


class Message(models.Model):
    MESSAGE_STATUSES = (('queued', 'Queued'),
                        ('sent', 'Sent'),
                        ('delivered', 'Delivered'),
                        ('undelivered', 'Undelivered'))
    created_at = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=255, choices=MESSAGE_STATUSES, default='queued')
    broadcast = models.ForeignKey(Broadcast, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='messages')

    def __str__(self):
        return f"Message {self.id}"
