import os
import time
import logging
import requests

from dotenv import load_dotenv
from requests.exceptions import RequestException

from broadcasts.models import Message

load_dotenv()

API_URL = os.getenv("API_URL")
ACCESS_TOKEN = os.getenv("ACCESS_TOKEN")

logger = logging.getLogger(__name__)


def send_message_to_client(phone_number, message_text, api_url=API_URL, access_token=ACCESS_TOKEN):
    message = Message.objects.get(client__phone_number=phone_number, broadcast__message_text=message_text)
    retries = 5
    delay = 5
    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {access_token}'
    }
    for i in range(retries):
        try:
            response = requests.post(
                api_url + str(message.pk),
                json={'id': message.pk, 'phone_number': phone_number, 'message_text': message_text},
                headers=headers, timeout=10)
            if response.status_code == 200:
                message.status = 'sent'
                message.save()
                logger.info(f'Sent message {message.pk} to client {phone_number}')
                break
            else:
                message.status = 'undelivered'
                message.save()
                logger.warning(f'Failed to send message {message.pk} to client {phone_number}')
        except RequestException:
            logger.exception(f'Error occurred while sending message {message.pk} to client {phone_number}')
        time.sleep(delay)
