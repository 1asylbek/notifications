from django.core.exceptions import ValidationError


def validate_phone_number(phone_number: str) -> bool:
    if not phone_number.startswith('7'):
        raise ValidationError('Phone number must start with 7')

    if not phone_number[1:].isdigit():
        raise ValidationError('Phone number must contain only digits')

    return True
