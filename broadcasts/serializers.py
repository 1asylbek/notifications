from rest_framework import serializers

from .models import Client, Broadcast, Message
from .validators import validate_phone_number


class BroadcastSerializer(serializers.ModelSerializer):
    class Meta:
        model = Broadcast
        fields = ('id', 'start_time', 'message_text', 'operator_code', 'tag', 'end_time')


class ClientSerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(validators=[validate_phone_number])

    class Meta:
        model = Client
        fields = ('id', 'phone_number', 'mobile_operator_code', 'tag', 'timezone')


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'created_at', 'status', 'broadcast', 'client')


