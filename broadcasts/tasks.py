from django.utils import timezone
from celery import shared_task
from .models import Broadcast, Client, Message
from .utils import send_message_to_client


@shared_task
def send_broadcast_messages(pk, eta=None):
    broadcast = Broadcast.objects.get(pk=pk)
    clients = Client.objects.filter(mobile_operator_code=broadcast.operator_code)

    local_time = timezone.localtime(timezone.now(), timezone.get_current_timezone())
    if broadcast.start_time <= local_time <= broadcast.end_time:
        for client in clients:
            message = Message.objects.create(
                broadcast=broadcast,
                client=client,
                status='queued'
            )
            send_message_to_client(client.phone_number, broadcast.message_text)
