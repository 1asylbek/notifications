from django.contrib import admin
from .models import (
    Client,
    Broadcast,
    Message
)


class ClientAdmin(admin.ModelAdmin):
    list_display = ('phone_number', 'mobile_operator_code', 'tag', 'timezone')
    list_filter = ('mobile_operator_code', 'tag')
    search_fields = ('mobile_operator_code', 'tag')


class BroadcastAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_time', 'message_text', 'operator_code', 'tag', 'end_time')
    list_filter = ('start_time', 'end_time', 'operator_code', 'tag')
    search_fields = ('message_text', 'operator_code', 'tag')


class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'status', 'broadcast', 'client')
    list_filter = ('status', 'created_at', 'broadcast', 'client')
    search_fields = ('status', 'broadcast__message_text')
    readonly_fields = ('created_at',)
    fields = ('status', 'broadcast', 'client')


admin.site.register(Client, ClientAdmin)
admin.site.register(Broadcast, BroadcastAdmin)
admin.site.register(Message, MessageAdmin)
