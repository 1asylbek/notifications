from django.utils import timezone
from rest_framework import viewsets, status
from .tasks import send_broadcast_messages

from .serializers import ClientSerializer, BroadcastSerializer, MessageSerializer
from .models import Client, Broadcast, Message

from rest_framework.response import Response
from django.shortcuts import get_object_or_404


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class BroadcastViewSet(viewsets.ModelViewSet):
    queryset = Broadcast.objects.all()
    serializer_class = BroadcastSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        broadcast = serializer.instance
        start_time = serializer.validated_data['start_time']
        if start_time > timezone.now():
            send_broadcast_messages.delay(broadcast.pk, eta=start_time)
        else:
            send_broadcast_messages(broadcast.pk)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class StatisticsViewSet(viewsets.ViewSet):
    def list(self, request):
        broadcasts = Broadcast.objects.all()
        broadcast_statistics = []
        for broadcast in broadcasts:
            sent_messages = Message.objects.filter(broadcast=broadcast, status='sent').count()
            delivered_messages = Message.objects.filter(broadcast=broadcast, status='delivered').count()
            undelivered_messages = Message.objects.filter(broadcast=broadcast, status='undelivered').count()
            broadcast_statistics.append({
                'id': broadcast.id,
                'sent': sent_messages,
                'delivered': delivered_messages,
                'undelivered': undelivered_messages
            })
        return Response({'statistics': broadcast_statistics})

    def retrieve(self, request, pk=None):
        broadcast = get_object_or_404(Broadcast, pk=pk)
        messages = Message.objects.filter(broadcast=broadcast)
        message_statistics = []
        for message in messages:
            message_statistics.append({
                'id': message.id,
                'client': message.client.phone_number,
                'status': message.status
            })
        return Response({'statistics': message_statistics})
